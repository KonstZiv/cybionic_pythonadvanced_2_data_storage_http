# CyBionic_PythonAdvanced_2_data_storage_HTTP

This is the material for lecture 2 of the IDTVN->PythonAdvanced training course. Main topics: CSV, JSON, XML, SQLite. Additional topics: Faker


---


[colab notebook](https://colab.research.google.com/drive/1Ib5zhOte3hV6rN40i3KYV0b_w-dVj1dr?usp=sharing)
